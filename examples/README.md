## greenlock cluster examples

First you need to change the email address in `examples/worker.js`.

Then you can run the example like so:

```
node examples/serve.js
```

That will put certificates in `~/letsencrypt.test` by default.
